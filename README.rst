

BuildStream with recc
=====================
This repository is a proof of concept of how we can run remote
execution aware tooling such as recc and eventually Bazel in the
deterministic BuildStream sandbox while giving these tools access
to CAS service (and eventually the other remote execution services)


How to run the demo
-------------------

* Install BuildStream from git using the ``tristan/bazel-experiment`` branch

* Clone this repository

* Run the ``./prepare.sh`` script, this will require ``docker`` to generate
  a base runtime for the experiment

* Run ``bst source track base.bst``, this is just to obtain the sha256sum of
  the base runtime we'll be using

* Run ``bst build example.bst``

To verify the results you'll need to observe the artifact log which will
contain debugging output from ``recc``:

.. code::

   bst artifact log example.bst

And you should be able to observe a cache hit at the end of the log:

.. code::

   2022-10-13T02:59:34.895+0000 [15:140393466015616] [executioncontext.cpp:379] [INFO] Action Cache hit for [a378dfdeb3125601c94f0405ef7330cfa27dac5b25f7820870d0b5063dc0060c/142]


Extended demo
-------------
For the hell of it, we added the ``example-buildbox-common.bst`` and added the
buildbox-common source code directly to this project's ``files/`` directory.

With this element we've enabled ``recc`` by default, and as such we can now
run ``bst build example-buildbox-common.bst`` such that it will use ``recc``.

Try building this element, and modifying source or header files in the
``files/buildbox-common`` directory in order to observe comparative build
times.
