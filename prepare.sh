#!/bin/sh

docker build docker -t bst-plugins-bazel-platform

container="$(docker container create bst-plugins-bazel-platform:latest /bin/bash)"

mkdir -p tarball && rm -f tarball/platform.tar

docker export "${container}" > tarball/platform.tar

docker rm "$container"
